var express = require('express');
var router = express.Router();
var biciletaController = require("../../controllers/API/bicicletaControllerAPI");


router.get('/', biciletaController.bicicleta_list);
router.post('/mostrarbici', biciletaController.bicicleta_id);
router.post('/create', biciletaController.bicicleta_create);
router.post('/update', biciletaController.bicicleta_update);
router.delete('/delete', biciletaController.bicicleta_delete);

module.exports= router;