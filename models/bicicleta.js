var Bicicleta = function (id,color,modelo,ubicacion) {
    this.id= id;
    this.color=color;
    this.modelo= modelo;
    this.ubicacion= ubicacion;
}

Bicicleta.prototype.toString = function() {
    return "ID: "+ this.id+" | Color: "+this.color;
}

Bicicleta.allBicis = [];

Bicicleta.add = function(aBici) {
    Bicicleta.allBicis.push(aBici);
}

Bicicleta.findById = function (aBiciId) {
    var aBici = Bicicleta.allBicis.find(x => x.id == aBiciId);
    if(aBici)
        return aBici;
    else 
        throw new Error('No existe la bicicleta'+ aBiciId);
}

Bicicleta.removeById = function(aBiciId){
    for (var i = 0;i < Bicicleta.allBicis.length;i++){
        if(Bicicleta.allBicis[i].id == aBiciId){
            Bicicleta.allBicis.splice(i,1);
            break;
        }
    }
}

var a = new Bicicleta("1","Rojo","BMX",[-34.599832, -58.650775]);
var b = new Bicicleta("2","Verde","Mountain",[-34.595244, -58.636033]);

Bicicleta.add(a);
Bicicleta.add(b);

module.exports = Bicicleta;